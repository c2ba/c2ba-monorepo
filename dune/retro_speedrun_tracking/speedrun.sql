WITH
speedrun_campaign AS (
    SELECT
        cycle,
        CONCAT('Speedrun ', CAST(cycle AS VARCHAR)) AS speedrun_number,
        CAST(start_lock_date AS TIMESTAMP) AS start_lock_date,
        CAST(end_lock_date AS TIMESTAMP) AS end_lock_date,
        CAST(start_accum_date AS TIMESTAMP) AS start_accum_date,
        CAST(end_accum_date AS TIMESTAMP) AS end_accum_date
    FROM (VALUES
        (1, '2024-11-26 00:00', '2024-12-26 23:59', '2024-11-26 00:00', '2024-12-26 23:59'),
        (2, '2024-12-27 00:00', '2025-01-23 05:00', '2024-12-27 00:00', '2025-01-27 09:00'),
        (3, '2025-01-23 05:01', '2025-02-19 23:59', '2025-01-27 10:00', '2025-02-20 15:30'),
        (4, '2025-02-20 00:00', '2025-03-19 23:59', '2025-02-20 16:00', '2025-03-19 23:59')
    ) AS t(cycle, start_lock_date, end_lock_date, start_accum_date, end_accum_date)
),
excluded_addresses AS (
    SELECT address FROM (VALUES
        (0x3a29cab2e124919d14a6f735b6033a3aad2b260f), -- oRETRO contract --
        (0x86b634eac93e463fcc303e632ddf05cfaadfdad1), -- bveRETRO contract --
        (0x883c0207519a6e0da4fbeb9f22427ccd902640a0), -- reward accumulation msig --
        (0x76B5fd0271e21C4D02698c3E8099916535b3fb80), -- speedrun msig --
        (0x32dac1b8ad93b53f549d6555e01c35dcc50b6229), -- liveRETRO veNFT --
        (0x35dcead4670161a3d123b007922d61378d3a9d18), -- team msig 1 --
        (0x8fc1d544eaa32f29274b93a80108f9f22807695c), -- team msig 2 --
        (0x1a8042DeD3d7B02929a1BEC785a5325B2E89EAd8), -- stabl treasury --
        (0xa876E99E295f34EF23180B5d0e8eE44FAfbCe9b4) -- CASH accum from oRETRO --
    ) AS t(address)
),
eligible_locked AS (
    SELECT
        speedrun_number,
        provider AS beneficiary,
        tokenId,
        (locktime - TO_UNIXTIME(evt_block_time)) / 62467200 AS lock_power,
        CAST(value AS DOUBLE)/1e18 AS locked_amount
    FROM retro_finance_polygon.VotingEscrow_evt_Deposit
    INNER JOIN speedrun_campaign ON evt_block_time BETWEEN start_lock_date AND end_lock_date
    WHERE (deposit_type = 1 OR deposit_type = 2)
    AND provider NOT IN (SELECT address FROM excluded_addresses)
),
veretro_per_beneficiary AS (
    SELECT
        speedrun_number,
        beneficiary,
        LISTAGG(DISTINCT CAST(tokenId AS VARCHAR), ', ') WITHIN GROUP (ORDER BY CAST(tokenId AS VARCHAR)) AS token_ids,
        SUM(locked_amount * lock_power) AS total_veretro_locked
    FROM (
        SELECT DISTINCT
            speedrun_number,
            beneficiary,
            tokenId,
            locked_amount,
            lock_power
        FROM eligible_locked
    ) AS distinct_locked
    WHERE lock_power > 0
    GROUP BY speedrun_number, beneficiary
),
exercise_data AS (
    SELECT
        speedrun_number,
        sender,
        LISTAGG(DISTINCT CAST(nftID AS VARCHAR), ', ') WITHIN GROUP (ORDER BY CAST(nftID AS VARCHAR)) AS nft_ids,
        SUM(TRY_CAST(amount AS DOUBLE) / 1e18) AS oretro
    FROM retro_finance_polygon.oRETRO_evt_ExerciseVe
    INNER JOIN speedrun_campaign ON evt_block_time BETWEEN start_lock_date AND end_lock_date
    WHERE sender NOT IN (SELECT address FROM excluded_addresses)
    GROUP BY sender, speedrun_number
),
liveretro_data AS (
    SELECT
        speedrun_number,
        sender AS liveretro_sender,
        SUM(TRY_CAST(amount AS DOUBLE) / 1e18) AS liveretro
    FROM abacus_polygon.LiveRetroManager_evt_Deposit
    INNER JOIN speedrun_campaign ON evt_block_time BETWEEN start_lock_date AND end_lock_date
    WHERE sender NOT IN (SELECT address FROM excluded_addresses)
    GROUP BY sender, speedrun_number
),
locker_wallets AS (
    SELECT
        COALESCE(v.speedrun_number, e.speedrun_number, l.speedrun_number) as speedrun_number,
        COALESCE(v.beneficiary, e.sender, l.liveretro_sender) as beneficiary,
        CASE
            WHEN e.nft_ids IS NOT NULL AND v.token_ids IS NOT NULL THEN CONCAT(v.token_ids, ', ', e.nft_ids)
            WHEN e.nft_ids IS NOT NULL THEN e.nft_ids
            ELSE v.token_ids
        END AS token_ids,
        COALESCE(v.total_veretro_locked, 0) AS total_veretro_locked,
        COALESCE(e.oretro, 0) AS oretro,
        COALESCE(l.liveretro, 0) AS liveretro,
        (COALESCE(v.total_veretro_locked, 0) + COALESCE(e.oretro, 0) + COALESCE(l.liveretro, 0)) AS total_amount_locked
    FROM veretro_per_beneficiary v
    FULL OUTER JOIN exercise_data e ON (v.beneficiary = e.sender AND v.speedrun_number = e.speedrun_number)
    FULL OUTER JOIN liveretro_data l ON (v.beneficiary = l.liveretro_sender AND v.speedrun_number = l.speedrun_number)
    AND beneficiary NOT IN (SELECT address FROM excluded_addresses)
),
eligible_wallets AS (
    SELECT
        *,
        CASE
            WHEN total_amount_locked > 60000
            THEN total_amount_locked
            ELSE 0
        END AS eligible_amount_locked
    FROM
        locker_wallets
),
retro_balance AS (
    SELECT
        speedrun_number,
        SUM(CASE
                WHEN "to" = FROM_HEX('0x883C0207519a6e0Da4fbeb9F22427ccD902640a0') THEN TRY_CAST(value AS DOUBLE) / 1e18
                WHEN "from" = FROM_HEX('0x883C0207519a6e0Da4fbeb9F22427ccD902640a0') THEN -TRY_CAST(value AS DOUBLE) / 1e18
                ELSE 0
            END) AS retro_balance
    FROM retro_finance_polygon.RETRO_evt_Transfer
    INNER JOIN speedrun_campaign ON evt_block_time BETWEEN start_accum_date AND end_accum_date
    WHERE contract_address = FROM_HEX('BFA35599c7AEbb0dAcE9b5aa3ca5f2a79624D8Eb')
    GROUP BY speedrun_number
),
bveretro_balance AS (
    SELECT
        speedrun_number,
        SUM(CASE
                WHEN "to" = FROM_HEX('0x883C0207519a6e0Da4fbeb9F22427ccD902640a0') THEN TRY_CAST(value AS DOUBLE) / 1e18
                WHEN "from" = FROM_HEX('0x883C0207519a6e0Da4fbeb9F22427ccD902640a0') THEN -TRY_CAST(value AS DOUBLE) / 1e18
                ELSE 0
            END) AS retro_balance
    FROM erc20_polygon.evt_transfer
    INNER JOIN speedrun_campaign ON evt_block_time BETWEEN start_accum_date AND end_accum_date
    WHERE contract_address = FROM_HEX('0x86b634EAC93E463fCc303E632dDF05CfAadFDaD1')
    GROUP BY speedrun_number
),
oretro_balance AS (
    SELECT
        speedrun_number,
        SUM(CASE
                WHEN "to" = FROM_HEX('0x883C0207519a6e0Da4fbeb9F22427ccD902640a0') THEN TRY_CAST(value AS DOUBLE) / 1e18
                WHEN "from" = FROM_HEX('0x883C0207519a6e0Da4fbeb9F22427ccD902640a0') THEN -TRY_CAST(value AS DOUBLE) / 1e18
                ELSE 0
            END) AS retro_balance
    FROM retro_finance_polygon.oRETRO_evt_Transfer
    INNER JOIN speedrun_campaign ON evt_block_time BETWEEN start_accum_date AND end_accum_date
    WHERE contract_address = FROM_HEX('0x3A29CAb2E124919d14a6F735b6033a3AaD2B260F')
    GROUP BY speedrun_number
),
cumulative_bveretro AS (
    SELECT
        b.speedrun_number,
        SUM(b.retro_balance) OVER (ORDER BY b.speedrun_number ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS cumulative_bveretro
    FROM bveretro_balance b
),
total_locked_sum AS (
    SELECT
        speedrun_number,
        SUM(eligible_amount_locked) AS sum_eligible_amount_locked
    FROM eligible_wallets
    GROUP BY speedrun_number
),
total_balances AS (
    SELECT
        COALESCE(rb.speedrun_number, cb.speedrun_number, ob.speedrun_number) AS speedrun_number,
        COALESCE(rb.retro_balance, 0) AS retro_balance,
        COALESCE(cb.cumulative_bveretro, 0) AS bveretro_balance,
        COALESCE(ob.retro_balance, 0) AS oretro_balance,
        (COALESCE(rb.retro_balance, 0) + COALESCE(cb.cumulative_bveretro, 0) + COALESCE(ob.retro_balance, 0)) AS total_balance
    FROM retro_balance rb
    FULL OUTER JOIN cumulative_bveretro cb ON rb.speedrun_number = cb.speedrun_number
    FULL OUTER JOIN oretro_balance ob ON rb.speedrun_number = ob.speedrun_number
),
final_data AS (
    SELECT
        e.speedrun_number,
        e.beneficiary,
        e.token_ids,
        e.total_veretro_locked,
        e.oretro,
        e.liveretro,
        e.total_amount_locked,
        e.eligible_amount_locked,
        tb.total_balance AS total_to_distribute,
        tb.retro_balance AS total_retro_to_distribute,
        tb.bveretro_balance AS total_bveretro_to_distribute,
        tb.oretro_balance AS total_oretro_to_distribute,
        (SELECT SUM(eligible_amount_locked) FROM eligible_wallets WHERE speedrun_number = e.speedrun_number) AS total_eligible_amount_locked
    FROM eligible_wallets e
    FULL OUTER JOIN total_balances tb ON e.speedrun_number = tb.speedrun_number
)
SELECT
    speedrun_campaign.speedrun_number,
    beneficiary,
    total_to_distribute * eligible_amount_locked / total_eligible_amount_locked AS veRETRO_reward,
    total_amount_locked,
    eligible_amount_locked,
    total_to_distribute * (eligible_amount_locked / total_eligible_amount_locked) / total_amount_locked AS veRETRO_reward_per_locked,
    token_ids,
    total_veretro_locked,
    oretro,
    liveretro,
    total_to_distribute,
    total_retro_to_distribute,
    total_bveretro_to_distribute,
    total_oretro_to_distribute,
    total_eligible_amount_locked,
    start_lock_date as speedrun_start_date,
    end_lock_date as speedrun_end_date
FROM final_data
FULL OUTER JOIN speedrun_campaign ON speedrun_campaign.speedrun_number = final_data.speedrun_number
ORDER BY speedrun_number DESC, total_amount_locked DESC
-- SELECT
--     tb.speedrun_number,
--     tb.total_balance AS total_to_distribute,
--     tb.total_balance / tls.sum_eligible_amount_locked AS distributed_balance_per_locked,
--     tb.veretro_balance,
--     tb.bveretro_balance,
--     tb.oretro_balance,
--     tls.sum_eligible_amount_locked AS eligible_locked_amount,
--     '0x883C0207519a6e0Da4fbeb9F22427ccD902640a0' AS distribution_wallet
-- FROM total_balances tb
-- INNER JOIN total_locked_sum tls
-- ON tb.speedrun_number = tls.speedrun_number
-- ORDER BY tb.speedrun_number DESC;
