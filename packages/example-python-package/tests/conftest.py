import os

import pytest

# TODO check if this is still needed, and move it to a dedicated pytest plugin
if os.getenv("_PYTEST_RAISE", "0") != "0":
    # See https://stackoverflow.com/questions/62419998/how-can-i-get-pytest-to-not-catch-exceptions # noqa
    # Allow to break on exception when debugging in VSCode
    # _PYTEST_RAISE is set to 1 in launch.json for the test launch request.
    @pytest.hookimpl(tryfirst=True)
    def pytest_exception_interact(
        call,
    ) -> None:
        raise call.excinfo.value

    @pytest.hookimpl(tryfirst=True)
    def pytest_internalerror(
        excinfo,
    ) -> None:
        raise excinfo.value
