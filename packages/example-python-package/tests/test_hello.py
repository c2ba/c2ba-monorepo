from python_example_package import hello


def test_hello():
    assert hello() == "Hello from example-python-package!"
