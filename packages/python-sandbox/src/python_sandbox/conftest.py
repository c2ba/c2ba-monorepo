from pathlib import Path

import dotenv

dotenv.load_dotenv()

PATH_PROJECT_ROOT = Path(__file__).parent.parent.parent
PATH_DATA_ROOT = PATH_PROJECT_ROOT / "data"
