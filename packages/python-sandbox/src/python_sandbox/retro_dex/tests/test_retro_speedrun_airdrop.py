import json
import os
from pathlib import Path

from dune_client.client import DuneClient
from pydantic import BaseModel
from python_sandbox.conftest import PATH_DATA_ROOT


class RetroSpeedrunAirdropRecipient(BaseModel):
    beneficiary: str
    veRETRO_reward: float
    eligible_amount_locked: float
    speedrun_number: str


class RetroSpeedrunAirdrop(BaseModel):
    recipients: list[RetroSpeedrunAirdropRecipient]


def test_retro_speedrun_airdrop():
    retro_speedrun_query_id = 4584802
    dune = DuneClient(os.environ["DUNE_API_KEY"])
    query_result = dune.get_latest_result(retro_speedrun_query_id)
    airdrop = RetroSpeedrunAirdrop.model_validate(
        {
            "recipients": [
                row for row in query_result.get_rows() if row["beneficiary"] is not None
            ]
        }
    )
    all_speedruns_recipients = sorted(
        [recipient for recipient in airdrop.recipients if recipient.veRETRO_reward > 0],
        key=lambda recipient: (recipient.speedrun_number, recipient.veRETRO_reward),
        reverse=True,
    )
    speedruns = set(
        [recipient.speedrun_number for recipient in all_speedruns_recipients]
    )
    for speedrun_number in speedruns:
        recipients = [
            recipient
            for recipient in all_speedruns_recipients
            if recipient.speedrun_number == speedrun_number
        ]
        total_to_distribute = sum(
            [recipient.veRETRO_reward for recipient in recipients]
        )
        total_locked = sum(
            [recipient.eligible_amount_locked for recipient in recipients]
        )
        print("total_to_distribute = ", total_to_distribute)
        print("total_locked = ", total_locked)
        shares = {
            recipient.beneficiary: recipient.eligible_amount_locked / total_locked
            for recipient in recipients
        }
        distribute = {
            recipient.beneficiary: {
                "share": shares[recipient.beneficiary],
                "to_distribute": total_to_distribute * shares[recipient.beneficiary],
                "to_distribute_query": recipient.veRETRO_reward,
            }
            for recipient in recipients
        }
        number = speedrun_number.split("Speedrun ")[-1]
        outfile = PATH_DATA_ROOT / "retro_speedrun_airdrop" / f"speedrun_{number}.json"
        outfile.parent.mkdir(parents=True, exist_ok=True)
        outfile.write_text(
            f"{json.dumps(distribute, indent=2)}\n",
            encoding="utf8",
            newline="\n",
        )
        outfile_text = outfile.parent / f"{outfile.stem}.txt"
        outfile_text.write_text(
            f"{'\n'.join(
                [
                    f"{beneficiary}={distribute[beneficiary]['to_distribute']}"
                    for beneficiary in distribute
                ]
            )}\n",
            encoding="utf8",
            newline="\n",
        )
