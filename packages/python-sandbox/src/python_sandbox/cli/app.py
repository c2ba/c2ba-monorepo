import typer
from python_sandbox.cli.git import Git
from python_sandbox.cli.git import make_app as make_git_app


def make_app(*, git: Git):
    app = typer.Typer()

    app.add_typer(make_git_app(git=git), name="git")

    @app.command()
    def shell():
        """Start an IPython shell with the Nomiks Builder API context."""
        import IPython
        from traitlets.config import Config

        cfg = Config()
        cfg.InteractiveShellApp.exec_lines = [
            "%load_ext autoreload",
            "%autoreload complete --log",
            "import httpx",
            "from pydantic import BaseModel",
            "from dataclasses import dataclass",
            "import numpy as np",
            "import pandas as pd",
            "import matplotlib.pyplot as plt",
            "import scipy",
            "import simpy",
            "import python_sandbox as ps",
        ]

        IPython.start_ipython(argv=[], header="Nomiks Builder API shell", config=cfg)

    return app
