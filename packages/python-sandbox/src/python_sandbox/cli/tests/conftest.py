import shlex
from dataclasses import dataclass
from unittest.mock import MagicMock

import pytest
import typer
from python_sandbox.cli.app import make_app
from python_sandbox.cli.git import Git
from typer.testing import CliRunner


@dataclass
class AppClient:
    cli_runner: CliRunner
    cli_app: typer.Typer

    def run(self, cmd_line: str):
        return self.cli_runner.invoke(self.cli_app, shlex.split(cmd_line))


@pytest.fixture()
def git_mock():
    return MagicMock(spec=Git)


@pytest.fixture()
def cli_app(git_mock):
    return make_app(git=git_mock)


@pytest.fixture()
def app_client(cli_app):
    return AppClient(CliRunner(), cli_app)
