import subprocess
from unittest.mock import call

from python_sandbox.cli.git import GitCli


def test_rebase_all_should_run_correct_git_command(app_client, git_mock):
    result = app_client.run("git rebase-all --onto newbase feature1 feature2")
    assert result.exit_code == 0
    git_mock.run.assert_has_calls(
        [
            call("rebase --update-refs --rebase-merges newbase feature1"),
            call("rebase --update-refs --rebase-merges newbase feature2"),
        ]
    )


def test_rebase_all_should_use_origin_main_as_default_branch(app_client, git_mock):
    result = app_client.run("git rebase-all feature1 feature2")
    assert result.exit_code == 0
    git_mock.run.assert_has_calls(
        [
            call("rebase --update-refs --rebase-merges origin/main feature1"),
            call("rebase --update-refs --rebase-merges origin/main feature2"),
        ]
    )


def test_git_cli_run_should_call_subprocess_run_with_correct_arguments():
    git = GitCli()
    cp = git.run("--version", stdout=subprocess.PIPE)
    assert cp.returncode == 0
    assert cp.stdout.startswith("git version")
