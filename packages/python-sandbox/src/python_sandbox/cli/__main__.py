from python_sandbox.cli.app import make_app
from python_sandbox.cli.git import GitCli

app = make_app(git=GitCli())

if __name__ == "__main__":
    app()
