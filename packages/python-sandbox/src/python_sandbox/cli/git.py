import shlex
import subprocess
from abc import ABC, abstractmethod
from functools import partial

import typer


class Git(ABC):
    @abstractmethod
    def run(self, command: str, *args, **kwargs) -> subprocess.CompletedProcess: ...


class GitCli(Git):
    def run(self, command: str, *args, **kwargs) -> subprocess.CompletedProcess:
        return subprocess.run(
            ["git", *shlex.split(command)],
            *args,
            **{
                "shell": True,
                "check": True,
                "universal_newlines": True,
                **kwargs,
            },
        )


def make_app(*, git: Git):
    app = typer.Typer(name="git")

    @app.command("rebase-all")
    def rebase_all(
        branches: list[str],
        onto: str = "origin/main",
    ):
        """
        Rebase multiple branches onto a base branch.
        """
        rebase_multiple(git=git, branches=branches, onto=onto)

    return app


def rebase_multiple(*, git: Git, branches: list[str], onto: str):
    # Useful options for rebase:
    # --update-refs
    #   Rebase all branches that are based on <oldbase> to <newbase>.
    # --rebase-merges
    #   Rebase merge commits

    for branch in branches:
        git.run(f"rebase --update-refs --rebase-merges {onto} {branch}")
