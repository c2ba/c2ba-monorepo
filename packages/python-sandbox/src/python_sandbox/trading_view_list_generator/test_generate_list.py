import json
import time
from pathlib import Path

import httpx

exclude = {
    "USDT",
    "STETH",
    "USDC",
    "WBTC",
    "DAI",
    "WEETH",
    "USDE",
    "EZETH",
    "FDUSD",
    "RETH",
    "METH",
    "EETH",
    "RSETH",
    "MSOL",
}


def test_generate_list():
    data_dir = Path(__file__).parent / "data"
    data_dir.mkdir(parents=True, exist_ok=True)
    cache = data_dir / "cache.json"
    if cache.exists():
        all_coins = json.loads(cache.read_text())
    else:
        all_coins = []
        for i in range(3):
            all_coins += httpx.get(
                f"https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=100&page={i+1}"
            ).json()
            time.sleep(1)
        cache.write_text(
            f"{json.dumps(all_coins, indent=2)}\n", encoding="utf-8", newline="\n"
        )

    base = [
        "BITSTAMP:BTCUSD",
        "COINBASE:ETHUSD",
        "ETHBTC",
        "TOTAL3/BTC",
        "OTHERS/BTC",
        "DXY",
    ]

    for coin in all_coins[2:101]:
        if coin["symbol"].upper() in exclude:
            continue
        base.append(f"###{coin['symbol'].upper()}")
        base.append(coin["symbol"].upper() + "USDT")
        base.append(coin["symbol"].upper() + "USDT/BITSTAMP:BTCUSD")
        base.append(coin["symbol"].upper() + "USDT/COINBASE:ETHUSD")

    (data_dir / "_____2025_CHASSE_ALTCOINS_2.txt").write_text(
        f"{'\n'.join(base)}\n", encoding="utf-8", newline="\n"
    )
