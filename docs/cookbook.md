# Cookbook

## How to add a python sub-project

- Run ```rye init [apps|packages]/{name}```
- Edit `pyproject.toml`
- Run ```rye sync```

## How to add a vite sub-project

- Run ```pnpm create vite [apps/packages]/{name}```
