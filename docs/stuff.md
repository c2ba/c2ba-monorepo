# TODO - Maybe / Someday

- python: ipython terminal + datascience hub + integration on system
- python: cython experiment + build with rye
- python: experiment vscode test runner with coverage

- react: clean archi from scratch
- react: do the base react tutorial
- react: reproduce shadcn example in a clean code context

- node: read eslint doc to understand base config in turborepo
- node: read typescript doc to understand base config in turborepo
- next: do the base next tutorial
