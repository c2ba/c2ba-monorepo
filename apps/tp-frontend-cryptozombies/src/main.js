import "./style.css";

// 1. Import modules.
import {
  createPublicClient,
  createWalletClient,
  custom,
  formatUnits,
  getAddress,
  getContract,
  http,
  parseAbiItem,
  parseUnits,
} from "viem";
import { sepolia } from "viem/chains";
import { UNI } from "./abi/UNI";

// 2. Set up your client with desired chain & transport.
const publicClient = createPublicClient({
  chain: sepolia,
  transport: http(),
});

const [account] = await window.ethereum.request({
  method: "eth_requestAccounts",
});
const walletClient = createWalletClient({
  account,
  chain: sepolia,
  transport: custom(window.ethereum),
});

const uniContract = getContract({
  address: "0x1f9840a85d5aF5bf1D1762F925BDADdC4201F984", // UNI address
  abi: UNI,
  client: { public: publicClient, wallet: walletClient },
});

const addr = getAddress(account);

// 3. Consume an action!
const blockNumber = await publicClient.getBlockNumber();
const decimals = await uniContract.read.decimals();

document.querySelector("#app").innerHTML = `
  <div>
    <p>Current block is <span id="blockNumber">${blockNumber}</span></p>
    <h1>Token ${await uniContract.read.symbol()}</h1>
    <p>Name: ${await uniContract.read.name()}</p>
    <p>Address: <a href="https://sepolia.etherscan.io/token/${uniContract.address}" target="_blank">${uniContract.address}</a></p>
    <p>Total Supply: ${formatUnits(await uniContract.read.totalSupply(), decimals)}</p>
    <p>Balance of ${addr}: <span id="balance">${formatUnits(await uniContract.read.balanceOf([addr]), decimals)}</span></p>
    <hr>
    <p>amount: <input id="amount" type="number" value="0"><button id="maxButton">max</button></p>
    <p>recipient: <input id="recipient" type="text" value="" placeholder="0x..."><button id="sendButton">send</button></p>
    <p id="txStatus"></p>
  </div>
`;

const unwatch = publicClient.watchBlockNumber({
  onBlockNumber: (blockNumber) =>
    (document.querySelector("#blockNumber").innerHTML = `${blockNumber}`),
});
document.querySelector("#maxButton").addEventListener("click", async () => {
  document.querySelector("#amount").value = formatUnits(
    await uniContract.read.balanceOf([addr]),
    decimals
  );
});
document.querySelector("#sendButton").addEventListener("click", async () => {
  const recipient = document.querySelector("#recipient").value;
  const amount = BigInt(
    parseUnits(document.querySelector("#amount").value, decimals)
  );
  const hash = await uniContract.write.transfer([recipient, amount]);
  document.querySelector("#txStatus").innerHTML =
    `Transaction sent: <a href="https://sepolia.etherscan.io/tx/${hash}" target="_blank">${hash}</a>`;
  const transaction = await publicClient.waitForTransactionReceipt({ hash });
  document.querySelector("#txStatus").innerHTML =
    `Transaction ${transaction.status}: <a href="https://sepolia.etherscan.io/tx/${hash}" target="_blank">${hash}</a>`;
});

publicClient.watchBlockNumber({
  onBlockNumber: async () => {
    const logs = await publicClient.getLogs({
      address: uniContract.address,
      event: parseAbiItem(
        "event Transfer(address indexed from, address indexed to, uint256 value)"
      ),
    });
    const myTransfers = logs.filter(
      (log) =>
        log.args.from == getAddress(account) ||
        log.args.to == getAddress(account)
    );
    if (myTransfers.length > 0) {
      document.querySelector("#balance").innerHTML = formatUnits(
        await uniContract.read.balanceOf([account]),
        decimals
      );
    }
  },
});
