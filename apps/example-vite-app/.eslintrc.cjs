/** @type {import("eslint").Linter.Config} */
module.exports = {
  root: true,
  extends: ["@c2ba/eslint-config/vite.js"],
};
