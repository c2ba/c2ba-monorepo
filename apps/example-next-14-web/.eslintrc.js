/** @type {import("eslint").Linter.Config} */
module.exports = {
  root: true,
  extends: ["@c2ba/eslint-config/next.js"],
  parser: "@typescript-eslint/parser",
  parserOptions: {
    project: true,
  },
};
